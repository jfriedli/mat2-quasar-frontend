describe('Info page', () => {
  beforeEach(() => {
    cy.visit('/info')
  })
  it('should have the correct text, links and titles', () => {
    cy.get('[data-cy=mat-locally-title]').contains('MAT2 locally')
    cy.get('[data-cy=mat-locally-info]').contains('certain about this: Act accordingly.')

    cy.get('[data-cy=mat-pip-title]').contains('MAT2 is available in pip')
    cy.get('[data-cy=mat-pip-code]').contains('pip3 install mat2')

    cy.get('[data-cy=mat-debian-title]').contains('MAT2 on Debian')
    cy.get('[data-cy=mat-debian-p]').contains('MAT2 is available on Debian.')
    cy.get('[data-cy=mat-debian-code]').contains('sudo apt install mat2')

    cy.get('[data-cy=debian-link]').contains('https://packages.debian.org/sid/mat2')
    cy.get('[data-cy=debian-link]')
      .should('have.attr', 'href').and(
        'include',
        'https://packages.debian.org/sid/mat2'
      )

    cy.get('[data-cy=supp-formats]').contains('Supported file formats')
    cy.get('[data-cy=supp-formats-chip]').contains('.mp3')
    cy.get('[data-cy=main-back-button]').should('be.visible')
    cy.get('[data-cy=main-back-button]').click()
    cy.url().should('not.contain', '/info')
  })
})

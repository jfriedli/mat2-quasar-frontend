describe('error page', () => {
  beforeEach(() => {
    cy.visit('/error')
  })
  it('should show the correct buttons and images', () => {
    cy.get('[data-cy=kitty-p]').find('img')
      .should('have.attr', 'src').and('include', 'img/kitty-error')
    cy.get('[data-cy=general-error-text]').contains('O Ooooh, Something went wrong')
    cy.get('[data-cy=general-error-report]').contains('If you keep getting this error report it')

    cy.get('[data-cy=error-btn] .q-btn__content > span').contains('Back')
    cy.get('[data-cy=error-btn]')
      .should('have.attr', 'href').and(
        'include',
        '/'
      )
    cy.get('[data-cy=main-back-button]').should('be.visible')
  })
})

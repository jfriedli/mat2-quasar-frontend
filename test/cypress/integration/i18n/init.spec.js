describe('Locale', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('should update correctly', () => {
    cy.get('h1').contains('Remove Metadata')
    cy.get('p').contains(
      'The file you see is just the tip of the iceberg. Remove the hidden metadata with MAT2'
    )
    cy.get('.uppy-Dashboard-AddFiles-title').contains(
      'Simply drag & drop, paste or use the file select button below.'
    )

    cy.get('[data-cy=locale-select]').click()
    cy.get('[data-cy=locale-select-item]').eq(0).click()

    cy.get('h1').contains('Metadaten entfernen')
    cy.get('p').contains(
      'Die sichtbare Datei ist nur die Spitze des Eisbergs. Entferne die versteckten Metadaten mit MAT2'
    )
    cy.get('.uppy-Dashboard-AddFiles-title').contains(
      'Drag & Drop, Einfügen oder die Dateiauswahl verwenden.'
    )
  })
})
